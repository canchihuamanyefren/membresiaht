<?php

if($_POST) {
    $cli_name = "";
    $cli_lastname="";
    $cli_dni = "";
    $cli_email = "";
    $cli_cel = "";
    $cli_dir = "";
    $idequipo = "";
    $cli_fecha = "";
    $idhora = "";
    $cli_message = "";

    if(isset($_POST['cli_name'])) {
      $cli_name = filter_var($_POST['cli_name'], FILTER_SANITIZE_STRING);
    }
        if(isset($_POST['cli_lastname'])) {
      $cli_lastname = filter_var($_POST['cli_lastname'], FILTER_SANITIZE_STRING);
    }
    if(isset($_POST['cli_dni'])) {
      $cli_dni = filter_var($_POST['cli_dni'], FILTER_SANITIZE_STRING);
    }


    if(isset($_POST['cli_email'])) {
        $cli_email = str_replace(array("\r", "\n", "%0a", "%0d"), '', $_POST['cli_email']);
        $cli_email = filter_var($cli_email, FILTER_VALIDATE_EMAIL);
    }

     if(isset($_POST['cli_cel'])) {
        $cli_cel = filter_var($_POST['cli_cel'], FILTER_SANITIZE_STRING);
    }

    if(isset($_POST['cli_dir'])) {
        $cli_dir = filter_var($_POST['cli_dir'], FILTER_SANITIZE_STRING);
    }

    if(isset($_POST['idequipo'])) {
        $idequipo = filter_var($_POST['idequipo'], FILTER_SANITIZE_STRING);
    }

    if(isset($_POST['cli_fecha'])) {
        $cli_fecha = filter_var($_POST['cli_fecha'], FILTER_SANITIZE_STRING);
    }

    if(isset($_POST['idhora'])) {
        $idhora = filter_var($_POST['idhora'], FILTER_SANITIZE_STRING);
    }

    if(isset($_POST['cli_message'])) {
        $cli_message = htmlspecialchars($_POST['cli_message']);
    }

    $headers  = 'Nombre: ' . $cli_name .
        "\n Apellido: " . $cli_lastname .
    "\n N° de DNI: " . $cli_dni .
    "\n E-mail: " . $cli_email .
    "\n Celular: " . $cli_cel .
    "\n Dirección: " . $cli_dir .
    "\n Equipo: " . $idequipo .
    "\n Fecha: " . $cli_fecha .
    "\n Hora: " . $idhora .
    "\n Referencia de Direccion: " . $cli_message;

}

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/Exception.php';
require 'PHPMailer/PHPMailer.php';
require 'PHPMailer/SMTP.php';


try {
    $mail = new PHPMailer(true);
    //Server settings
    //$mail->SMTPDebug = 0; //SMTP::DEBUG_SERVER;                      // Enable verbose debug output
    //$mail->isSMTP();                                            // Send using SMTP
    //$mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
    //$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
    //$mail->Username   = 'adautoyefrengustavo@gmail.com';                     // SMTP username
    //$mail->Password   = 'Y3fr3n258';                               // SMTP password
    $mail->SMTPSecure = 'tls';//PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
    //$mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
$mail->From="visita@membresiaht.com";
$mail->FromName="Solicitud de informacion";

    //Recipients
    //$mail->setFrom($cli_email, $cli_name);
    $mail->addAddress('sistemas2@digident.com.pe');
      $mail->addAddress('servicio@digident.com.pe');      // Add a recipient
   $mail->addAddress('ventas@digident.com.pe'); 
     $mail->addAddress('Teo@htdent.com'); 
    // Attachments
    //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

    // Content
    //$mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Solicitud de informacion';
    $mail->Body    = $headers;

    $mail->send();
    echo '<script>
        alert("Gracias por contactarnos. Recibirá una respuesta dentro de las 24 horas.");
           var url = "http://www.membresiaht.com/membresia/visita.html";
window.location.href = url;
        </script>';


}

catch (Exception $e) {
    echo "El mensaje no pudo ser enviado. Error de envío: {$mail->ErrorInfo}";
}
?>
