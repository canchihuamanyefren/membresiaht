jQuery(document).ready(function($) {
	$('.imghover').hover(function() {
		$(this).find('img').before('<div class="capahover" style="height:' + $(this).height() + 'px; width:' + $(this).width() + 'px"></div>');
		var mitadancho = ($(this).width() / 2) - 30;
		var mitadalto = ($(this).height() / 2) - 30;
		$('.capahover').css({
			'background-position-x' : mitadancho,
			'background-position-y' : mitadalto
		});
	}, function() {
		$('.capahover').remove();
	});
}); 