<?php

if($_POST) {
    $idequipo = "";
    $cli_name = "";
    $cli_lastname="";
    $cli_dni = "";
    $cli_cop = "";
    $cli_date = "";
    $cli_cel = "";
    $cli_email = "";
    $cli_dir = "";
    $cli_ciudad = "";
    $cli_provincia = "";
    $cli_nom_clinica = "";
    $cli_ruc ="";

    if(isset($_POST['idequipo'])) {
        $idequipo = filter_var($_POST['idequipo'], FILTER_SANITIZE_STRING);
    }

    if(isset($_POST['cli_name'])) {
      $cli_name = filter_var($_POST['cli_name'], FILTER_SANITIZE_STRING);
    }
        if(isset($_POST['cli_lastname'])) {
      $cli_lastname = filter_var($_POST['cli_lastname'], FILTER_SANITIZE_STRING);
    }

    if(isset($_POST['cli_dni'])) {
        $cli_dni = filter_var($_POST['cli_dni'], FILTER_SANITIZE_STRING);
    }

    if(isset($_POST['cli_cop'])) {
        $cli_cop = filter_var($_POST['cli_cop'], FILTER_SANITIZE_STRING);
    }

    if(isset($_POST['cli_date'])) {
        $cli_date = filter_var($_POST['cli_date'], FILTER_SANITIZE_STRING);
    }

    if(isset($_POST['cli_cel'])) {
        $cli_cel = filter_var($_POST['cli_cel'], FILTER_SANITIZE_STRING);
    }

    if(isset($_POST['cli_email'])) {
        $cli_email = str_replace(array("\r", "\n", "%0a", "%0d"), '', $_POST['cli_email']);
        $cli_email = filter_var($cli_email, FILTER_VALIDATE_EMAIL);
    }

    if(isset($_POST['cli_dir'])) {
        $cli_dir = filter_var($_POST['cli_dir'], FILTER_SANITIZE_STRING);
    }

    if(isset($_POST['cli_ciudad'])) {
        $cli_ciudad = filter_var($_POST['cli_ciudad'], FILTER_SANITIZE_STRING);
    }

    if(isset($_POST['cli_provincia'])) {
        $cli_provincia = filter_var($_POST['cli_provincia'], FILTER_SANITIZE_STRING);
    }

    if(isset($_POST['cli_nom_clinica'])) {
        $cli_nom_clinica = filter_var($_POST['cli_nom_clinica'], FILTER_SANITIZE_STRING);
    }

    if(isset($_POST['cli_ruc'])) {
        $cli_ruc = filter_var($_POST['cli_ruc'], FILTER_SANITIZE_STRING);
    }


    $headers  = 'Nombre: ' . $cli_name.
    "\n Apellido: " . $cli_lastname .
    "\n Equipo: " . $idequipo .
    "\n N° de DNI: " . $cli_dni .
    "\n N° de COP: " . $cli_cop .
    "\n Fecha de Nacimiento: " . $cli_date .
    "\n Celular: " . $cli_cel .
    "\n E-mail: " . $cli_email .
    "\n Dirección: " . $cli_dir .
    "\n Ciudad: " . $cli_ciudad .
    "\n Provincia: " . $cli_provincia .
    "\n Nombre de Clinica: " . $cli_nom_clinica .
    "\n N° de RUC: " . $cli_ruc;

}

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/Exception.php';
require 'PHPMailer/PHPMailer.php';
require 'PHPMailer/SMTP.php';



try {
    $mail = new PHPMailer();
    //Server settings
    //$mail->SMTPDebug = 0; //SMTP::DEBUG_SERVER;                      // Enable verbose debug output
    //$mail->isSMTP();                                            // Send using SMTP
    //$mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
    //$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
    //$mail->Username   = 'adautoyefrengustavo@gmail.com';                     // SMTP username
//    $mail->Password   = 'Y3fr3n258';                               // SMTP password
    $mail->SMTPSecure = 'tls';//PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
  //  $mail->Port       = 587;              // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
$mail->From="reserva@membresiaht.com";
$mail->FromName="Reservacion de Membresia HT";



    //Recipients
   // $mail->setFrom($cli_email, $cli_name);
    $mail->addAddress('sistemas2@digident.com.pe');
   $mail->addAddress('servicio@digident.com.pe');      // Add a recipient
    $mail->addAddress('ventas@digident.com.pe'); 
    $mail->addAddress('Teo@htdent.com'); 
    // Name is optional
    // Attachments
    //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

    // Content
  //  $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Reserva de Equipo';
    $mail->Body    = $headers;

    $mail->send();
    echo '<script>
        alert("Gracias por contactarnos. Recibirá una respuesta dentro de las 24 horas.");
   var url = "http://www.membresiaht.com/membresia/reservar.html";
window.location.href = url;
        </script>';

} catch (Exception $e) {
    echo "El mensaje no pudo ser enviado. Error de envío: {$mail->ErrorInfo}";
}
?>


<?php

?>
